using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManagerWave : MonoBehaviour
{
    public bool pasarRonda;
    // Bandera para indicar si el juego termin�
    public static bool gameOverWave = false;

    // Bandera para indicar si el jugador gan�
    public static bool gameWinWave = false;

    [Header("Scripts")]

    // referencia al controlador de dialogo
    [SerializeField] ControlDialogos Dialogador;

    // Referencia al generador de enemigos
    [SerializeField] Enemy_Spawner es;

    // Referencia al gestor de la tienda
    [SerializeField] Manager_Tienda managerTienda;

    //Referencia al jugador
    [SerializeField] Player_Movement Jugador;

    // Prefabs de botiqu�n y escudo
    public GameObject botiquinPrefab;
    public GameObject escudoPrefab;

    // Men� de pausa del juego
    public GameObject menuPausa;

    // Puntos actuales del jugador
    public int puntos;

    // L�mite m�ximo de puntos que el jugador puede alcanzar
    public int limitePuntos = 999999;


    // Contadores relacionados con los enemigos y sus muertes
    private int enemigosAsesinados = 0;
    private int enemigosAsesinadosEscudo = 0;

    // L�mites para generar botiquines y escudos
    private int limite = 10;
    private int limiteEscudos = 20;

    // �ltima posici�n donde muri� un enemigo
    private Vector3 ultimaPosicionEnemigoMuerto;

    // Bandera para verificar si el juego est� pausado
    public bool juegoPausado = false;

    // Contador de tiempo restante para la ronda
    public int contador = 40;

    // nuevo valor del contador despues de cada ronda (inician iguales)
    public int SubirContador = 40;

    // N�mero de rondas completadas
    public int rondasPasadas = 0;

    void Start()
    {
        // Reiniciar las banderas de estado del juego
        gameOverWave = false;
        gameWinWave = false;
        StartCoroutine(decrementarTiempo());
    }

    void Update()
    {
        //controla la condicion de victoria y derrota
        VictoriaDerrota();

        // controla la pausa acorde la variable
        ControlPausa();


        // Recargar la escena actual si se presiona la tecla 'R'
        if (Input.GetKeyDown(KeyCode.R))
        {
            Time.timeScale = 1;
            ReloadCurrentScene();
        }

        // Si el contador llega a 0, abrir la tienda de forma automatica
        if (contador <= 0 && managerTienda.tiendaActivada == false)

        {
            rondasPasadas = rondasPasadas + 1;

            Dialogador.Felicitar = true;
            Dialogador.Dialogando = true;
            Dialogador.Instruccion = 1;
            Dialogador.PresentarSigEnemigo = true;
            
            managerTienda.tiendaActivada = true;

                        
        }

        // activar pausa con cartel de pausa si se preciona escape
        PausarJuegoBoton(); 
    }


    void ganarPuntos(int cantidad)
    {
        // Aumentar los puntos del jugador sin exceder el l�mite establecido
        if (puntos + cantidad >= limitePuntos)
        {
            puntos = limitePuntos;
        }
        else
        {
            puntos += cantidad;
        }
    }

    // verifica la vida del jugador y las rondas ganadas 
    void VictoriaDerrota()
    {
        if (Jugador.vida <= 0)
        {
          gameOverWave = true;
        }

        if (rondasPasadas >= 6)
        {
          gameWinWave = true;
        }

        if (gameWinWave)
        {
            juegoPausado = true;
        }

        if (gameOverWave)
        {
            juegoPausado = true;
        }

    }

    // pausar juego con carte, presionando escape
    void PausarJuegoBoton()
    {
        // Alternar entre pausar y reanudar el juego al presionar 'Escape'
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (juegoPausado)
            {
                // Desactivar el men� de pausa y reanudar el tiempo
                menuPausa.SetActive(false);
                Time.timeScale = 1f;
            }
            else
            {
                // Activar el men� de pausa y detener el tiempo
                menuPausa.SetActive(true);
                Time.timeScale = 0f;
            }

            // Invertir el estado de pausa
            juegoPausado = !juegoPausado;
        }
    } 

    // pausa juego sin cartel, dependiendo solo de variable
    void ControlPausa()
    {
        // control de la pausa
        if (juegoPausado == false)
        {
            Time.timeScale = 1f;
        }

        if (juegoPausado == true)
        {
            Time.timeScale = 0f;
        }


    } 
   
    public void SiguienteRonda()
    {
        managerTienda.cerrarTienda();
        juegoPausado = false;

        // sumamos 5 seg y reiniciamos el contador para la nueva ronda
        SubirContador = SubirContador + 5;
        contador = SubirContador;

        // SUMAMOS UNA RONDA GANADA
       // rondasPasadas += 1;

        // REINICIAR COORUTINAS
        StopCoroutine(es.spawnEnemigos());
        StopCoroutine(es.spawnBomba());
        StopCoroutine(decrementarTiempo());

        StartCoroutine(es.spawnEnemigos());
        StartCoroutine(es.spawnBomba());
        StartCoroutine(decrementarTiempo());

        pasarRonda = true;
        es.LimpiarEnemigosRestantes();
    } 
    
    public void CerrarTienda()
    {
        managerTienda.cerrarTienda();
        SiguienteRonda();
    }

    public void instanciarBotiquines(Vector3 posicion)
    {
        // Almacenar la �ltima posici�n de un enemigo muerto
        ultimaPosicionEnemigoMuerto = posicion;

        // Incrementar el contador de enemigos asesinados
        enemigosAsesinados++;

        // Si se alcanza el l�mite, generar un botiqu�n y reiniciar el contador
        if (enemigosAsesinados >= limite)
        {
            Instantiate(botiquinPrefab, ultimaPosicionEnemigoMuerto, Quaternion.identity);
            enemigosAsesinados = 0;
        }
    }

    public void instanciarEscudos()
    {
        // Incrementar el contador de enemigos asesinados para escudos
        enemigosAsesinadosEscudo++;

        // Si se alcanza el l�mite, generar un escudo y reiniciar el contador
        if (enemigosAsesinadosEscudo >= limiteEscudos)
        {
            Vector3 posicionEscudo = new Vector3(0f, 1.5f, 0f);
            Instantiate(escudoPrefab, posicionEscudo, Quaternion.identity);
            enemigosAsesinadosEscudo = 0;
        }
    }

    public void ReloadCurrentScene()
    {
        // Recargar la escena actual
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void agregarPuntos(int cantidadPuntos)
    {
        // Incrementar los puntos del jugador
        puntos += cantidadPuntos;
    }

    public IEnumerator decrementarTiempo()
    {
        // Reducir el contador cada segundo mientras sea mayor a 0
        while (contador > 0)
        {
            yield return new WaitForSeconds(1f); // Esperar 1 segundo
            contador--; // Decrementar el contador
        }
    }
        
}
