using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Misil_Explotar : MonoBehaviour
{
    public GameObject explosionPrefab;
    public float radioExplosion = 5f;
    public float fuerzaExplosion = 100f;
    public float dpsMisil = 100f;

    void Start()
    {
        Invoke("DestruirBala", 3);
    }

    private void OnTriggerEnter(Collider other)
    {
        Collider[] colliders = Physics.OverlapSphere(transform.position, radioExplosion);

        if (other.CompareTag("Wall"))
        {
            Destroy(gameObject);
        }

        if (other.CompareTag("Enemy") || other.CompareTag("EnemyBomba") || other.CompareTag("EnemyGrande") || other.CompareTag("ViejoVerde") || other.CompareTag("DobleCara") || other.CompareTag("viejaHolograma"))
        {
            AplicarDaņoMisil();
            DestruirBala();
        }
    }

    private void AplicarDaņoMisil()
    {
        Collider[] colliders = Physics.OverlapSphere(transform.position, radioExplosion);

        foreach (Collider collider in colliders)
        {
            if (collider.CompareTag("Enemy") || collider.CompareTag("EnemyGrande"))
            {
                Enemy_Movement enemy = collider.GetComponent<Enemy_Movement>();
                if (enemy != null)
                {
                    enemy.recibirDaņo(dpsMisil);
                }
            }

            if (collider.CompareTag("EnemyBomba"))
            {
                Bomba_Movement enemy = collider.GetComponent<Bomba_Movement>();
                if (enemy != null)
                {
                    enemy.recibirDaņo(dpsMisil);
                }
            }

            if (collider.CompareTag("ViejoVerde"))
            {
                ViejoVerde enemy = collider.GetComponent<ViejoVerde>();
                if (enemy != null)
                {
                    enemy.recibirDaņo(dpsMisil);
                }
            }

            if (collider.CompareTag("DobleCara"))
            {
                Enemy_Cambiaformas enemy = collider.GetComponent<Enemy_Cambiaformas>();
                if (enemy != null)
                {
                    enemy.recibirDaņo(dpsMisil);
                }
            }

            if (collider.CompareTag("viejaHolograma"))
            {
                ViejaHologramas enemy = collider.GetComponent<ViejaHologramas>();
                if (enemy != null)
                {
                    enemy.recibirDaņo(dpsMisil);
                }
            }

            Rigidbody rb = collider.GetComponent<Rigidbody>();
            if (rb != null)
            {
                rb.AddExplosionForce(fuerzaExplosion, transform.position, radioExplosion);
            }
        }

        Vector3 explosionPosition = transform.position;
        Instantiate(explosionPrefab, explosionPosition, Quaternion.identity);
    }

    private void DestruirBala()
    {
        Destroy(gameObject);
    }

    //if (other.gameObject.TryGetComponent<Enemy_Movement>(out Enemy_Movement enemy))
    //{
    //    enemy.recibirDaņo(dpsMisil);
    //    Vector3 explosionPosition = transform.position;
    //    Instantiate(explosionPrefab, explosionPosition, Quaternion.identity);
    //    Destroy(gameObject);
    //}
}
