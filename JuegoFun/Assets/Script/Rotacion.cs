using UnityEngine;

public class Rotacion : MonoBehaviour
{
    // Velocidad de rotaci�n en grados por segundo
    public float velocidadRotacion = 50f;

    // Direcci�n de rotaci�n: true para derecha, false para izquierda
    public bool girarDerecha = true;

    void Update()
    {
        // Determina la direcci�n de la rotaci�n
        float direccion = girarDerecha ? 1 : -1;

        // Calcula la cantidad de rotaci�n en el eje Z para este frame
        float rotacionEnEsteFrame = direccion * velocidadRotacion * Time.deltaTime;

        // Aplica la rotaci�n al objeto en el eje Z
        transform.Rotate(0, 0, rotacionEnEsteFrame);
    }
}
