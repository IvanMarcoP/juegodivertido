using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_Shooting : MonoBehaviour
{
    [Header("Datos Arma")]
    public GameObject balaPrefab;
    public GameObject balaMetralletaPrefab;
    public GameObject balaEscopetaPrefab;
    public float balaSpeed;
    public Transform posicionDisparo;
    public GameObject brazoAdicional;
    public Transform posicionDisparo2;
    public bool brazoAdicionalActivo = false;
    public bool metralletaAdicionalActiva = false;

    [Header("Balas")]
    public int metralletaBalas = 100;
    public int escopetaBalas = 6;
    public int misilesBalas = 5;
    public int numeroArma = 1;

    [Header("Datos Lanza Misiles")]
    public GameObject misilPrefab;
    public float fuerzaLanzamiento;
    public Transform posicionLanzamiento;

    private Player_LaserGun plg;
    private bool puedeDisparar = true;
    private Manager_Tienda mt;
    private bool activarDisparo = true;

    void Awake()
    {
        plg = GetComponent<Player_LaserGun>();
    }

    void Start()
    {
        mt = GameObject.Find("ManagerTienda").GetComponent<Manager_Tienda>();
        brazoAdicional.SetActive(false);
        brazoAdicionalActivo = false;
        metralletaAdicionalActiva = false;
        //plg = GetComponent<Player_LaserGun>();
    }

    // Update is called once per frame
    void Update()
    {
        cambiarArma();
        armaActualDispara();

        if (mt.tiendaActivada == true)
        {
            activarDisparo = false;
        }
        else
        {
            activarDisparo = true;
        }

        if(brazoAdicionalActivo == true)
        {
            brazoAdicional.SetActive(true);
        }
    }

    private void armaActualDispara()
    {
        if (numeroArma == 1)
        {
            if (Input.GetMouseButtonDown(0))
            {
                DispararPistola();
            }
        }
        else if (numeroArma == 2)
        {
            if (Input.GetMouseButtonDown(0) && metralletaBalas > 0)
            {
                InvokeRepeating("DispararMetralleta", 0f, 0.1f); // Dispara cada 0.01 segundos
            }
            if (Input.GetMouseButtonUp(0))
            {
                CancelInvoke("DispararMetralleta");
            }
        }
        else if (numeroArma == 3)
        {
            if (Input.GetMouseButton(0) && escopetaBalas > 0)
            {
                DispararEscopeta();
            }
        }
        else if (numeroArma == 4)
        {
            if (Input.GetMouseButton(0) && plg.laserEnergia > 0)
            {
                plg.Activar();
            }
            else if (Input.GetMouseButtonUp(0) || plg.laserEnergia <= 0)
            {
                plg.Desactivar();
            }
        }
        else if (numeroArma == 5)
        {
            if (Input.GetMouseButtonDown(0) && misilesBalas > 0)
            {
                LanzarMisil();
            }
        }
    }

    private void cambiarArma()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            numeroArma = 1;
        }
        if (mt.metralletaON == true && Input.GetKeyDown(KeyCode.Alpha2))
        {
            numeroArma = 2;
        }
        if (mt.escopetaON == true && Input.GetKeyDown(KeyCode.Alpha3))
        {
            numeroArma = 3;
        }
        if (mt.laserOn == true && Input.GetKeyDown(KeyCode.Alpha4))
        {
            numeroArma = 4;
        }
        if (mt.misilesON == true && Input.GetKeyDown(KeyCode.Alpha5))
        {
            numeroArma = 5;
        }

    }

    #region Tipos de Disparo
    private void DispararPistola()
    {
        GameObject nuevaBala = Instantiate(balaPrefab, posicionDisparo.position, transform.rotation);
        Rigidbody nuevaBalaRb = nuevaBala.GetComponent<Rigidbody>();

        nuevaBalaRb.velocity = transform.forward * balaSpeed;

        if (brazoAdicionalActivo == true)
        {
            DispararPistolaAdional();
        }
    }

    private void DispararPistolaAdional()
    {
        GameObject nuevaBala = Instantiate(balaPrefab, posicionDisparo2.position, transform.rotation);
        Rigidbody nuevaBalaRb = nuevaBala.GetComponent<Rigidbody>();

        nuevaBalaRb.velocity = transform.forward * balaSpeed;
    }

    private void DispararMetralleta()
    {
        if(activarDisparo == true)
        {
            if (puedeDisparar && numeroArma == 2)
            {
                GameObject nuevaBala = Instantiate(balaMetralletaPrefab, posicionDisparo.position, transform.rotation);
                Rigidbody nuevaBalaRb = nuevaBala.GetComponent<Rigidbody>();

                nuevaBalaRb.velocity = transform.forward * balaSpeed;
                metralletaBalas--;

                if(metralletaAdicionalActiva == true)
                {
                    DisparoMetralletaAdicional();
                }

                if (metralletaBalas == 0)
                {
                    CancelInvoke("DispararMetralleta");
                }

                puedeDisparar = false;
                StartCoroutine(delayDisparo(0.01f));
            }
        }
    }

    private void DisparoMetralletaAdicional()
    {
        GameObject nuevaBala = Instantiate(balaMetralletaPrefab, posicionDisparo2.position, transform.rotation);
        Rigidbody nuevaBalaRb = nuevaBala.GetComponent<Rigidbody>();

        nuevaBalaRb.velocity = transform.forward * balaSpeed;

    }

    private void DispararEscopeta()
    {
        if(activarDisparo == true)
        {
            if (puedeDisparar && numeroArma == 3 && escopetaBalas > 0)
            {
                for (int i = 0; i < 13; i++)
                {
                    Vector3 direccion = Quaternion.Euler(Random.Range(-5f, 5f), Random.Range(-20f, 20f), Random.Range(-5f, 5f)) * transform.forward;
                    direccion += transform.forward;
                    direccion.Normalize();

                    GameObject nuevaBala = Instantiate(balaEscopetaPrefab, posicionDisparo.position, transform.rotation);
                    Rigidbody nuevaBalaRb = nuevaBala.GetComponent<Rigidbody>();

                    nuevaBalaRb.velocity = direccion * balaSpeed;
                }

                escopetaBalas--;

                puedeDisparar = false;
                StartCoroutine(delayDisparo(1f));
            }
        }
    
    }

    private void LanzarMisil()
    {
        GameObject misil = Instantiate(misilPrefab, posicionLanzamiento.position, transform.rotation);
        Rigidbody misilRb = misil.GetComponent<Rigidbody>();

        misilRb.velocity = transform.forward * fuerzaLanzamiento;
        misilesBalas--;
    }
    #endregion

    private IEnumerator delayDisparo(float tiempo)
    {
        yield return new WaitForSeconds(tiempo);
        puedeDisparar = true;
    }
}
