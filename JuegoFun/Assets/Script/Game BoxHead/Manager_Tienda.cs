using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Manager_Tienda : MonoBehaviour
{
    
    [Header("Datos y Scripts")]
    public GameObject menuMejoras;
    [SerializeField] Player_Movement pm;
    [SerializeField] Player_Shooting ps;
    [SerializeField] Player_LaserGun plg;
    [SerializeField] GameManagerWave gmw;
    [SerializeField] Enemy_Spawner es;
    [SerializeField] Bala_Destroy balaDamage;

    [Header("Botones Comprar Arma")]
    public Button botonMejoras;
    [SerializeField] Button escopetaBoton;
    [SerializeField] Button metralletaBoton;
    [SerializeField] Button laserBoton;
    [SerializeField] Button MisilesBoton;
    [SerializeField] Button brazoNuevoPistola;
    [SerializeField] Button metralletaNueva;

    [Header("Botones Comprar Balas o Mejoras")]
    [SerializeField] Button balasAK;
    [SerializeField] Button balasEscopeta;
    [SerializeField] Button energiaLaser;
    [SerializeField] Button misilesBalas;
    [SerializeField] Button dañoBalas;
    [SerializeField] Button dañoLaser;
    [SerializeField] Button botonVelocidad;
  

    [Header("Activar Arma")]
    public bool metralletaON = false;
    public bool escopetaON = false;
    public bool laserOn = false;
    public bool misilesON = false;
    private Player_TirarGranada ptg;

    [Header("Precios Balas")]
    [SerializeField] int aumento = 100;
    public int granadaPrecio = 500;
    public int precioBalasAK = 1000;
    public int precioBalasEscopeta = 1000;
    public int precioBalasLaser = 2000;
    public int precioBalasMisiles = 2000;
    public bool tiendaActivada = false;

    [Header("Precio Stats")]
    public int vidaPrecio = 1000;
    public int velocidadPrecio = 1000;
    public int aumentarDañoPrecio = 5000;
    public int aumentarLaserDaño = 5000;

    [Header("Precios Armas")]
    [SerializeField] int MetralletaArmaPrecio = 5000;
    [SerializeField] int EscopetataArmaPrecio = 10000;
    [SerializeField] int LaserArmaPrecio = 50000;
    [SerializeField] int LanzamisilesArmaPrecio = 100000;
    [SerializeField] int precioBrazo = 100000;
    [SerializeField] int precioMetralletaAdicional = 200000;

    void Start()
    {
        metralletaON = false;
        escopetaON = false;
        laserOn = false;
        misilesON = false;

        //botonesHijos.AddRange(conjuntoBotones.GetComponentsInChildren<Button>());

        ptg = GameObject.Find("Player").GetComponent<Player_TirarGranada>();
    }

    public void comprarEscopeta()
    {
        if(gmw.puntos >= EscopetataArmaPrecio)
        {
            escopetaON = true;
            escopetaBoton.interactable = false;
            gmw.puntos -= EscopetataArmaPrecio;
        }
    }

    public void comprarMetralleta()
    {
        if (gmw.puntos >= MetralletaArmaPrecio)
        {
            metralletaON = true;
            metralletaBoton.interactable = false;
            gmw.puntos -= MetralletaArmaPrecio;
        }
    }

    public void comprarLaser()
    {
        if (gmw.puntos >= LaserArmaPrecio)
        {
            laserOn = true;
            laserBoton.interactable = false;
            gmw.puntos -= LaserArmaPrecio;
        }
    }

    public void comprarMisiles()
    {
        if (gmw.puntos >= LanzamisilesArmaPrecio)
        {
            misilesON = true;
            MisilesBoton.interactable = false;
            gmw.puntos -= LanzamisilesArmaPrecio;
        }
    }

    public void comprarGranada()
    {
        if (gmw.puntos >= granadaPrecio)
        {
            gmw.puntos -= granadaPrecio;
            ptg.cantidadGranadas++;
            granadaPrecio = granadaPrecio + aumento;
        }
    }

    public void ComprarVida()
    {
        if (gmw.puntos >= vidaPrecio)
        {
            gmw.puntos -= vidaPrecio;
            pm.vidaMaxima+= 10;
            pm.vida = pm.vidaMaxima;
            vidaPrecio = vidaPrecio + aumento;

            Hud_ControllerWave.instance.sliderVida.value = pm.vida;
        }
    }

    public void comprarVelocidad()
    {
        if (gmw.puntos >= velocidadPrecio)
        {
            gmw.puntos -= velocidadPrecio;
            pm.velocidad += 0.50f;
            ps.balaSpeed += 0.50f;
            velocidadPrecio = velocidadPrecio + aumento;
            //conteoVelocidad++;
        }

        //if (conteoVelocidad == 5)
        //{
        //    botonVelocidad.interactable = false;
        //}
    }

    public void comprarDañoBalas()
    {
        if (gmw.puntos >= aumentarDañoPrecio)
        {
            gmw.puntos -= aumentarDañoPrecio;
            Bala_Destroy.ActualizarDaño(Bala_Destroy.dañoInfligido + 5f);
            aumentarDañoPrecio = aumentarDañoPrecio + aumento * 2;
        }
    }

    public void comprarDañoLaser()
    {
        if (gmw.puntos >= aumentarLaserDaño)
        {
            gmw.puntos -= aumentarLaserDaño;
            plg.dpsRayo += 20f;
            aumentarLaserDaño = aumentarLaserDaño + aumento * 2;
        }
    }    

    public void comprarBalasAK()
    {
        if(gmw.puntos >= precioBalasAK)
        {
            gmw.puntos -= precioBalasAK;
            ps.metralletaBalas += 30;
            precioBalasAK = precioBalasAK + aumento;
        }
    }

    public void comprarBalasEscopeta()
    {
        if (gmw.puntos >= precioBalasEscopeta)
        {
            gmw.puntos -= precioBalasEscopeta;
            ps.escopetaBalas += 8;
            precioBalasEscopeta = precioBalasEscopeta + aumento;
        }
    }

    public void comprarEnergiaLaser()
    {
        if (gmw.puntos >= precioBalasLaser)
        {
            gmw.puntos -= precioBalasLaser;
            plg.laserEnergia += 25;
            precioBalasLaser = precioBalasLaser + aumento * 2;
        }
    }

    public void comprarBalasMisiles()
    {
        if (gmw.puntos >= precioBalasMisiles)
        {
            gmw.puntos -= precioBalasMisiles;
            ps.misilesBalas += 5;
            precioBalasMisiles = precioBalasMisiles + aumento * 2;
        }
    }

    public void comprarBrazoAdicional()
    {
        if (gmw.puntos >= precioBrazo)
        {
            gmw.puntos -= precioBrazo;
            ps.brazoAdicionalActivo = true;
            brazoNuevoPistola.interactable = false;
        }
    }

    public void comprarMetralletaAdicional()
    {
        if (gmw.puntos >= precioMetralletaAdicional)
        {
            gmw.puntos -= precioMetralletaAdicional;
            ps.metralletaAdicionalActiva = true;
            metralletaNueva.interactable = false;
        }
           
    }

    public void abrirTienda()
    {
        gmw.juegoPausado = true;
        menuMejoras.SetActive(true);
        tiendaActivada = true;
        
    }

    public void cerrarTienda()
    {

        menuMejoras.SetActive(false);     
        tiendaActivada = false;
    }

}
