using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_LaserGun : MonoBehaviour
{
    [Header("Datos laser")]
    [SerializeField] private LineRenderer lr;
    public float dpsRayo = 100f;
    public float rango = 100f;
    public float laserEnergia = 100;
    public Transform disparoPosicion;
    public GameObject inicioLaser;
    public Transform efectoPosicion;

    void Awake()
    {
        lr.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        if(laserEnergia <= 0)
        {
            laserEnergia = 0;
        }
    }

    void FixedUpdate()
    {
        if(!lr.enabled)
        {
            return;
        }

        Ray ray = new Ray(disparoPosicion.position, disparoPosicion.forward);
        bool cast = Physics.Raycast(ray, out RaycastHit hit, rango);
        Vector3 hitPosition = cast ? hit.point : disparoPosicion.position + disparoPosicion.forward * rango;

        lr.SetPosition(0, disparoPosicion.position);
        lr.SetPosition(1, hitPosition);

        if (cast)
        {
            Enemy_Movement enemy = hit.collider.GetComponent<Enemy_Movement>();
            Bomba_Movement bomba = hit.collider.GetComponent<Bomba_Movement>();
            ViejoVerde viejoVerde = hit.collider.GetComponent<ViejoVerde>();
            Enemy_Cambiaformas dobleCara = hit.collider.GetComponent<Enemy_Cambiaformas>();
            ViejaHologramas viejaHologramas = hit.collider.GetComponent<ViejaHologramas>();
            if (enemy != null)
            {
                enemy.recibirDaņo(dpsRayo * Time.deltaTime);
            }
            else if(bomba != null)
            {
                bomba.recibirDaņo(dpsRayo * Time.deltaTime);
            }
            else if (viejoVerde != null)
            {
                viejoVerde.recibirDaņo(dpsRayo * Time.deltaTime);
            }
            else if (dobleCara != null)
            {
                dobleCara.recibirDaņo(dpsRayo * Time.deltaTime);
            }
            else if (viejaHologramas != null)
            {
                viejaHologramas.recibirDaņo(dpsRayo * Time.deltaTime);
            }
        }
    }

    public void Activar()
    {
        lr.enabled = true;
        laserEnergia -= Time.deltaTime * 10;
        GameObject punta = Instantiate(inicioLaser, efectoPosicion.position, efectoPosicion.rotation);
        Destroy(punta, 0.1f);
    }
    public void Desactivar()
    {
        lr.enabled = false;

        lr.SetPosition(0, disparoPosicion.position);
        lr.SetPosition(1, disparoPosicion.position);
    }
}
