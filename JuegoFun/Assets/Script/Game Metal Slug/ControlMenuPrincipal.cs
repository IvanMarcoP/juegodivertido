using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ControlMenuPrincipal : MonoBehaviour
{
    public GameObject BotonesNiveles;
    public GameObject Controles;
    public GameObject MenuInicial;
    public GameObject BotonesIniciales;
    public GameObject ColeccionEnemigos;

    private void Start()
    {

    }


    public void Play()
    {
       MenuInicial.SetActive(false);
       ColeccionEnemigos.SetActive(false);
        BotonesIniciales.SetActive(false);
        BotonesNiveles.SetActive(true);
    } // mostrar selector de niveles

    public void Controls()
    {
        MenuInicial.SetActive(false);
        ColeccionEnemigos.SetActive(false);
        BotonesIniciales.SetActive(false);
        Controles.SetActive(true);
    } // mostrar selector de niveles

    public void VolverAlMenu()
    {
        Controles.SetActive(false);
        BotonesNiveles.SetActive(false);
        MenuInicial.SetActive(true);
        ColeccionEnemigos.SetActive(true);
        BotonesIniciales.SetActive(true);
        
    }

    public void CargarNivel1()
    {
        SceneManager.LoadScene("GameWave");
    }

    public void CargarNivel2()
    {
        SceneManager.LoadScene("GameWave1");
    }

    public void CargarNivel3()
    {
        SceneManager.LoadScene("GameWave2");
    }

    public void Exit()
    {
        Application.Quit();
    }
}
