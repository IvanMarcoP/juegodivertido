using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Granada_Destroy : MonoBehaviour
{
    public float radioExplosion = 5f;
    public float fuerzaExplosion = 100f;
    public LayerMask capasAfectadas; // Opcional: Permite definir las capas afectadas por la explosión

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Enemy"))
        {
            // Obtener el script del enemigo y reducir su vida
            Enemy_Movimiento enemigo = collision.gameObject.GetComponent<Enemy_Movimiento>();
            enemigo.recibirDañoGranada();
        }
        else if (collision.gameObject.CompareTag("Floor"))
        {
            // Obtener todas las colisiones en el radio de explosión
            Collider[] colliders = Physics.OverlapSphere(transform.position, radioExplosion, capasAfectadas);

            // Aplicar daño o efectos a los objetos afectados
            foreach (Collider collider in colliders)
            {
                Rigidbody rb = collider.GetComponent<Rigidbody>();
                if (rb != null)
                {
                    // Aplicar fuerza explosiva a los objetos con Rigidbody
                    rb.AddExplosionForce(fuerzaExplosion, transform.position, radioExplosion);
                }

                if (collider.CompareTag("Enemy"))
                {
                    // Obtener el script del enemigo y reducir su vida
                    Enemy_Movimiento enemigo = collider.GetComponent<Enemy_Movimiento>();
                    enemigo.recibirDañoGranada();
                }
            }
        }

        // Destruir la granada
        Destroy(gameObject);
    }
}
