using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_LanzaMisiles : MonoBehaviour
{
    [Header("Datos Lanza Misiles")]
    public GameObject misilPrefab;
    public GameObject explosionPrefab;
    public float fuerzaLanzamiento;
    public float tiempoDetonacion;
    public float radioExplosion = 5f;
    public float fuerzaExplosion = 100f;
    public int cantidadMisiles = 10;
    public Transform posicionLanzamiento;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
