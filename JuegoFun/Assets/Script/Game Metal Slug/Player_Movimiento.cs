using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_Movimiento : MonoBehaviour
{
    [Header("Datos Personaje")]
    public int vidas;
    public int balas = 100;

    [Header("Datos Movimiento")]
    public float velocidad;

    [Header("Datos Salto")]
    public float fuerzaSalto;
    public float saltoCD;
    public LayerMask piso;
    public float distanciaPiso = 0.7f;
    bool puedeSalto;
    private Rigidbody rb;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        puedeSalto = true;
    }

    // Update is called once per frame
    void Update()
    {
        Movimiento();
        CharacterInputs();
    }

    private void Movimiento()
    {
        Vector3 vectorInput = new Vector3(0, 0, 0);

        if (Input.GetKey(KeyCode.D))
        {
            vectorInput.x = +1;
        }
        if (Input.GetKey(KeyCode.A))
        {
            vectorInput.x = -1;
        }

        vectorInput = vectorInput.normalized;

        Vector3 moverDireccion = new Vector3(vectorInput.x, 0, 0);
        transform.position += moverDireccion * velocidad * Time.deltaTime;

        float velRotacion = 30f;
        transform.forward = Vector3.Slerp(transform.forward, moverDireccion, Time.deltaTime * velRotacion);
    }

    private void CharacterInputs()
    {
        //Salto alto (Mantener)
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (EstaEnPiso() && puedeSalto)
            {
                puedeSalto = false;
                Salto();
                Invoke(nameof(ResetSalto), saltoCD);
            }
        }

        //Salto Corto (Tocar 1 vez)
        if(Input.GetKeyUp(KeyCode.Space) && rb.velocity.y > 0f)
        {
            puedeSalto = false;
            rb.velocity = new Vector3(rb.velocity.x, rb.velocity.y * 0.7f, rb.velocity.z);
            Invoke(nameof(ResetSalto), saltoCD);
        }
    }

    private bool EstaEnPiso()
    {
        return Physics.Raycast(transform.position, Vector3.down, distanciaPiso);
    }

    private void Salto()
    {
        rb.AddForce(Vector3.up * fuerzaSalto, ForceMode.Impulse);
        //rb.AddForce(new Vector3(0, fuerzaSalto, 0), ForceMode.Impulse);
    }

    private void ResetSalto()
    {
        puedeSalto = true;
    }

    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("Enemy"))
        {
            vidas = vidas - 1;
        }
    }
}

////Movimiento Mira
//if (Input.GetKeyDown(KeyCode.A))
//{
//    MoverArmaAtras();
//}
//else if (Input.GetKeyDown(KeyCode.D))
//{
//    RestaurarPosicionArma();
//}
//else 
//else if (Input.GetKeyUp(KeyCode.W))
//{
//    RestaurarPosicionArma();
//}

#region Mira
//private void MoverArmaAtras()
//{
//    posicionArma.localPosition = new Vector3(-posicionOriginalArma.x, posicionOriginalArma.y, posicionOriginalArma.z);
//    posicionArma.localRotation = Quaternion.Euler(0f, 180f, 0f); // Rota 180 grados sobre el eje Y
//}

//private void RestaurarPosicionArma()
//{
//    posicionArma.localPosition = posicionOriginalArma;
//    posicionArma.localRotation = rotacionOriginalArma;
//}


#endregion


//private void Disparar()
//{
//    GameObject nuevaBala = Instantiate(bala, posicionArma.position, Quaternion.identity);
//    Rigidbody nuevaBalaRb = nuevaBala.GetComponent<Rigidbody>();

//    Vector3 direccionDisparo;

//    if (posicionArma.localPosition.x < 0)
//    {
//        // Empty en la izquierda, dispara hacia la izquierda
//        direccionDisparo = Vector3.left;
//    }
//    else if (posicionArma.localPosition.x > 0)
//    {
//        // Empty en la derecha, dispara hacia la derecha
//        direccionDisparo = Vector3.right;
//    }
//    else if (Input.GetKey(KeyCode.W))
//    {
//        // Empty en posici�n vertical, dispara hacia arriba
//        direccionDisparo = Vector3.up;
//    }
//    else
//    {
//        // Empty en posici�n original, dispara hacia adelante
//        direccionDisparo = transform.forward;
//    }

//    // Convertir la direcci�n al plano 2D
//    direccionDisparo.z = 0f;

//    nuevaBalaRb.velocity = direccionDisparo.normalized * fuerzaDisparo;
//}


//Datos para el Arma

//private Quaternion rotacionOriginalArma;
//private Vector3 direccionDisparo;

//posicionArma = transform.Find("PosicionArma");

//rotacionOriginalArma = posicionArma.localRotation;