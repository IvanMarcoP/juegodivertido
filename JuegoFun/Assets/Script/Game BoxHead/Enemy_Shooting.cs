using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_Shooting : MonoBehaviour
{
    public GameObject balaEnemy;
    public Transform puntoDisparo;
    public float balaSpeed = 10f;
    public float fireRate = 2f;
    private float siguienteDisparo;

    // Start is called before the first frame update
    void Start()
    {
        siguienteDisparo = Time.time + 1f / fireRate;
    }

    // Update is called once per frame
    void Update()
    {
        if (Time.time >= siguienteDisparo)
        {
            Shoot();
            siguienteDisparo = Time.time + 1f / fireRate;
        }
    }

    void Shoot()
    {
        //GameObject nuevaBullet = Instantiate(balaEnemy, puntoDisparo.position, Quaternion.identity);
        //Rigidbody nuevaBalaRB = nuevaBullet.GetComponent<Rigidbody>();

        //nuevaBalaRB.velocity = transform.forward * balaSpeed;
        Instantiate(balaEnemy, puntoDisparo.position, puntoDisparo.rotation);
    }
}
