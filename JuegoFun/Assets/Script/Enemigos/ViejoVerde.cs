using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ViejoVerde : MonoBehaviour
{
    // Vida y velocidad del enemigo
    public float hp;
    public int velocidad;
    public int puntosGanados;
    // Referencia al jugador
    private GameObject jugador;
    private GameManagerWave gmw;
    private bool destruido = false;

    // Inicializa el enemigo encontrando al jugador
    void Start()
    {
        jugador = GameObject.Find("Player");
        gmw = GameObject.Find("GameManagerWave").GetComponent<GameManagerWave>();
    }

    // Mueve al enemigo hacia el jugador cada frame
    void Update()
    {
        // El enemigo mira al jugador y se mueve hacia �l
        transform.LookAt(jugador.transform);
        transform.Translate(velocidad * Vector3.forward * Time.deltaTime);
    }

    // M�todo para recibir da�o con cantidad variable
    public void recibirDa�o(float da�o)
    {
        if (destruido) return;

        hp -= da�o;

        if (hp <= 0)
        {
            destruido = true;
            this.desaparecer();
        }
    }

    // Destruye al enemigo y a�ade puntos al jugador
    private void desaparecer()
    {
        Vector3 posicionEnemigo = transform.position;

        Destroy(gameObject);

        if (gmw.puntos + puntosGanados >= gmw.limitePuntos)
        {
            gmw.puntos = gmw.limitePuntos;
        }
        else
        {
            gmw.agregarPuntos(puntosGanados);
        }
        gmw.instanciarBotiquines(posicionEnemigo);
        gmw.instanciarEscudos();
    }


    // Corutina que restaura la etiqueta original despu�s de 6 segundos
    //private IEnumerator VolverEtiquetaOriginal()
    //{
    //    yield return new WaitForSeconds(6);
    //    gameObject.tag = "ViejoVerde";
    //    Debug.Log("Etiqueta restaurada a ViejoVerde");
    //}

    // Detecta colisiones con otros objetos
    //private void OnCollisionEnter(Collision collision)
    //{
    //    //if (collision.gameObject.CompareTag("Player"))
    //    //{
    //    //    // Cambia el tag del enemigo a "Enemy" si colisiona con el jugador
    //    //    gameObject.tag = "Enemy";
    //    //    Debug.Log("Cambio de TAG a Enemy");

    //    //    StartCoroutine(VolverEtiquetaOriginal());
    //    //}

    //}
}
