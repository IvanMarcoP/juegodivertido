using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EsconderParedes : MonoBehaviour
{
    private Camera mainCamera; // C�mara principal que observa al jugador
    public LayerMask layerMask; // Capa de los objetos que pueden bloquear la c�mara
    public float transparency = 0.3f; // Nivel de transparencia (0 = invisible, 1 = opaco)

    private List<Renderer> currentRenderers = new List<Renderer>();
    private Dictionary<Renderer, Material[]> originalMaterials = new Dictionary<Renderer, Material[]>();

    void Update()
    {
        mainCamera = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();
        // Verificar si la c�mara est� asignada
        if (mainCamera == null)
        {
            Debug.LogWarning("C�mara no asignada al script TransparentObjects.");
            return;
        }

        // Direcci�n y distancia entre el jugador y la c�mara
        Vector3 direction = mainCamera.transform.position - transform.position;
        float distance = direction.magnitude;

        // Realizar un raycast para detectar objetos entre el jugador y la c�mara
        RaycastHit[] hits = Physics.RaycastAll(transform.position, direction, distance, layerMask);

        // Restaurar materiales de los objetos que ya no bloquean la vista
        foreach (var renderer in currentRenderers)
        {
            if (renderer != null && originalMaterials.ContainsKey(renderer))
            {
                renderer.materials = originalMaterials[renderer];
            }
        }
        currentRenderers.Clear();
        originalMaterials.Clear();

        // Aplicar transparencia a los objetos que bloquean la vista
        foreach (var hit in hits)
        {
            Renderer renderer = hit.collider.GetComponent<Renderer>();
            if (renderer != null && !originalMaterials.ContainsKey(renderer))
            {
                // Guardar los materiales originales
                originalMaterials[renderer] = renderer.materials;

                // Crear materiales transparentes
                Material[] transparentMaterials = new Material[renderer.materials.Length];
                for (int i = 0; i < renderer.materials.Length; i++)
                {
                    transparentMaterials[i] = new Material(renderer.materials[i]);
                    MakeMaterialTransparent(transparentMaterials[i]);
                }

                renderer.materials = transparentMaterials;
                currentRenderers.Add(renderer);
            }
        }
    }

    private void MakeMaterialTransparent(Material material)
    {
        // Configurar el material para ser transparente
        material.SetFloat("_Mode", 3); // Modo transparente
        material.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.SrcAlpha);
        material.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
        material.SetInt("_ZWrite", 0);
        material.DisableKeyword("_ALPHATEST_ON");
        material.EnableKeyword("_ALPHABLEND_ON");
        material.DisableKeyword("_ALPHAPREMULTIPLY_ON");
        material.renderQueue = 3000;

        // Reducir la opacidad del material
        Color color = material.color;
        color.a = transparency; // Ajustar el canal alfa
        material.color = color;
    }
}
