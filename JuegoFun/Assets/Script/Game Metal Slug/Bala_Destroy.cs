using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bala_Destroy : MonoBehaviour
{
    public static float dañoInfligido = 10f;
    private float tiempoBala = 3f;

    void Start()
    {
        Invoke("DestruirBala", tiempoBala);
    }

    public static void ActualizarDaño(float nuevoDaño)
    {
        dañoInfligido = nuevoDaño;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Wall"))
        {
            Destroy(gameObject);
        }

        if (other.gameObject.TryGetComponent<Enemy_Movement>(out Enemy_Movement enemy))
        {
            enemy.recibirDaño(dañoInfligido);
            Destroy(gameObject);
        }

        if (other.gameObject.TryGetComponent<Bomba_Movement>(out Bomba_Movement enemyBomba))
        {
            enemyBomba.recibirDaño(dañoInfligido);
            Destroy(gameObject);
        }

        if (other.gameObject.TryGetComponent<ViejoVerde>(out ViejoVerde viejoVerde))
        {
            viejoVerde.recibirDaño(dañoInfligido);
            Destroy(gameObject);
        }

        if (other.gameObject.TryGetComponent<Enemy_Cambiaformas>(out Enemy_Cambiaformas cambiaForma))
        {
            cambiaForma.recibirDaño(dañoInfligido);
            Destroy(gameObject);
        }

        if (other.gameObject.TryGetComponent<ViejaHologramas>(out ViejaHologramas viejaHologramas))
        {
            viejaHologramas.recibirDaño(dañoInfligido);
            Destroy(gameObject);
        }
    }

    private void DestruirBala()
    {
        Destroy(gameObject);
    }
}
