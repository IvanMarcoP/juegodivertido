using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Hud_ControllerWave : MonoBehaviour
{
    public static Hud_ControllerWave instance;

    // referencia al codigo que controla el ciclo de juego
    public GameManagerWave ControlJuego;
 

    [Header("Textos")]
    public TMPro.TMP_Text contadorTxt;
    public TMPro.TMP_Text textoPuntos;
    public TMPro.TMP_Text gameOverTxt;
    public TMPro.TMP_Text granadaText;
    public TMPro.TMP_Text balasTxt;

    [Header("Textos con aumento y Slider")]
    public TMPro.TMP_Text monedaAumento;
    public TMPro.TMP_Text vidaAumento;
    public TMPro.TMP_Text velocidadAumento;
    public TMPro.TMP_Text balasAK;
    public TMPro.TMP_Text balasEscopeta;
    public TMPro.TMP_Text energiaLaser;
    public TMPro.TMP_Text balasMisiles;
    public TMPro.TMP_Text dañoBalaAumento;
    public TMPro.TMP_Text dañoLaserAumento;
    public Slider sliderVida;
    public Slider tiempoContador;

    [Header("Armas imagenes")]
    [SerializeField] Image pistola;
    [SerializeField] Image escopeta;
    [SerializeField] Image metralleta;
    [SerializeField] Image laser;
    [SerializeField] Image lanzaMisiles;

    [Header("Scripts")]
    [SerializeField] Player_Movement pm;
    [SerializeField] Player_Shooting ps;
    [SerializeField] Player_TirarGranada ptg;
    [SerializeField] Player_LaserGun plg;
    [SerializeField] Manager_Tienda mt;
    [SerializeField] Enemy_Spawner es;


    //[Header("Puntos")]
    //public int puntos;

    void Awake()
    {
        instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        gameOverTxt.enabled = false;
        mt = GameObject.Find("ManagerTienda").GetComponent<Manager_Tienda>();
        ControlJuego = GameObject.Find("GameManagerWave").GetComponent<GameManagerWave>();
    }

    // Update is called once per frame
    void Update()
    {
       

        // actualiza el maximo valor del value
        tiempoContador.maxValue = ControlJuego.SubirContador;

        // actualiza el valor acorde al contador del juego
        tiempoContador.value = ControlJuego.contador;

        setearTextos();
        Perder();
        sliderVida.value = pm.vida;


    }

    private void Perder()
    {
        // si el jugador perdio mostramos el cartel de perder
        if(GameManagerWave.gameOverWave)
        {
            gameOverTxt.enabled = true;            
        }
    }

    private void setearTextos()
    {
        //vida y puntos
        textoPuntos.text = "        : " + ControlJuego.puntos.ToString("N0");
        sliderVida.maxValue = pm.vidaMaxima;

        //Granada
        granadaText.text = "   X " + ptg.cantidadGranadas.ToString();
        monedaAumento.text = mt.granadaPrecio.ToString();

        //vida y velocidad precios
        vidaAumento.text = mt.vidaPrecio.ToString();
        velocidadAumento.text = mt.velocidadPrecio.ToString();

        //Contador
        //contadorTxt.text = "Tiempo Restante " + es.contador.ToString();

        //comprar balas
        balasAK.text = mt.precioBalasAK.ToString();
        balasEscopeta.text = mt.precioBalasEscopeta.ToString();
        energiaLaser.text = mt.precioBalasLaser.ToString();
        balasMisiles.text = mt.precioBalasMisiles.ToString();

        //aumento de daño
        dañoBalaAumento.text = mt.aumentarDañoPrecio.ToString();
        dañoLaserAumento.text = mt.aumentarLaserDaño.ToString();

        if (ps.numeroArma == 1)
        {
            balasTxt.gameObject.SetActive(true);
            balasTxt.text = "   X " + "\u221E";
            pistola.gameObject.SetActive(true);
            metralleta.gameObject.SetActive(false);
            escopeta.gameObject.SetActive(false);
            laser.gameObject.SetActive(false);
            lanzaMisiles.gameObject.SetActive(false);
        }
        else if (ps.numeroArma == 2)
        {
            balasTxt.gameObject.SetActive(true);
            balasTxt.text = "    X " + ps.metralletaBalas.ToString();
            pistola.gameObject.SetActive(false);
            metralleta.gameObject.SetActive(true);
            escopeta.gameObject.SetActive(false);
            laser.gameObject.SetActive(false);
            lanzaMisiles.gameObject.SetActive(false);
        }
        else if(ps.numeroArma == 3)
        {
            balasTxt.gameObject.SetActive(true);
            balasTxt.text = "    X " + ps.escopetaBalas.ToString();
            pistola.gameObject.SetActive(false);
            metralleta.gameObject.SetActive(false);
            escopeta.gameObject.SetActive(true);
            laser.gameObject.SetActive(false);
            lanzaMisiles.gameObject.SetActive(false);
        }
        else if (ps.numeroArma == 4)
        {
            balasTxt.gameObject.SetActive(true);
            balasTxt.text = "       X " + plg.laserEnergia.ToString("F0");
            pistola.gameObject.SetActive(false);
            metralleta.gameObject.SetActive(false);
            escopeta.gameObject.SetActive(false);
            lanzaMisiles.gameObject.SetActive(false);
            laser.gameObject.SetActive(true);
        }
        else if (ps.numeroArma == 5)
        {
            balasTxt.gameObject.SetActive(true);
            balasTxt.text = "       X " + ps.misilesBalas.ToString();
            pistola.gameObject.SetActive(false);
            metralleta.gameObject.SetActive(false);
            escopeta.gameObject.SetActive(false);
            laser.gameObject.SetActive(false);
            lanzaMisiles.gameObject.SetActive(true);
        }

    }

}
