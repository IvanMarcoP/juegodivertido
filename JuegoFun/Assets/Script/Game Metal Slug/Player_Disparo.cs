using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_Disparo : MonoBehaviour
{
    public GameObject balaPrefab;
    public float balaSpeed;
    public Transform posicionDisparo;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.J))
        {
            Disparar();
        }
    }

    private void Disparar()
    {
        GameObject nuevaBala = Instantiate(balaPrefab, posicionDisparo.position, Quaternion.identity);
        Rigidbody nuevaBalaRb = nuevaBala.GetComponent<Rigidbody>();

        nuevaBalaRb.velocity = transform.forward * balaSpeed;

        if (Input.GetKey(KeyCode.W))
        {
            //posicionDisparo.position = new Vector3(-0.5f, 1f, 0f);
            nuevaBalaRb.velocity = transform.up * balaSpeed;
        }
        else
        {
            nuevaBalaRb.velocity = transform.forward * balaSpeed;
        }
    }
}
