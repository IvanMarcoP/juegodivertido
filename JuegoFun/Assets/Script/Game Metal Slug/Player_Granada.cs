using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_Granada : MonoBehaviour
{
    public GameObject granadaPrefab;
    public float fuerzaLanzamiento;
    public float tiempoDetonacion;
    public float radioExplosion = 5f;
    public float fuerzaExplosion = 100f;
    public Transform posicionLanzamiento;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.K))
        {
            LanzarGranada();
        }
    }

    private void LanzarGranada()
    {
        GameObject granada = Instantiate(granadaPrefab, posicionLanzamiento.position, Quaternion.identity);
        Rigidbody granadaRb = granada.GetComponent<Rigidbody>();

        Vector3 direccionLanzamiento = transform.forward + Vector3.up;

        granadaRb.AddForce(direccionLanzamiento * fuerzaLanzamiento, ForceMode.VelocityChange);

        granadaRb.rotation = Quaternion.LookRotation(direccionLanzamiento);
        // Detonar la granada despu�s de cierto tiempo
        StartCoroutine(DetonarGranada(granada, tiempoDetonacion));
    }

    private IEnumerator DetonarGranada(GameObject granada, float tiempo)
    {
        yield return new WaitForSeconds(tiempo);

        // Obtener todas las colisiones en el radio de explosión
        Collider[] colliders = Physics.OverlapSphere(granada.transform.position, radioExplosion);

        // Aplicar daño o efectos a los objetos afectados
        foreach (Collider collider in colliders)
        {
            if (collider.CompareTag("Enemy"))
            {
                // Obtener el script del enemigo y reducir su vida
                Enemy_Movimiento enemigo = collider.GetComponent<Enemy_Movimiento>();
                enemigo.recibirDañoGranada();
            }
            else
            {
                Rigidbody rb = collider.GetComponent<Rigidbody>();
                if (rb != null)
                {
                    // Aplicar fuerza explosiva a los objetos con Rigidbody
                    rb.AddExplosionForce(fuerzaExplosion, granada.transform.position, radioExplosion);
                }
            }
        }

        // Destruir la granada
        Destroy(granada);
    }
}
