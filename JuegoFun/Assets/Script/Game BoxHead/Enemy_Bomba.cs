using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_Bomba : MonoBehaviour
{
    public float tiempoVida = 5;
    public float radioExplosion = 5f;
    public float fuerzaExplosion = 100f;
    public GameObject explosionPrefab;

    private float tiempoActual = 0f;
    //private bool explotado = false;

    // Update is called once per frame
    void Update()
    {
        tiempoActual += Time.deltaTime;

        // Si ha pasado el tiempo de vida, explotar
        if (tiempoActual >= tiempoVida)
        {
            Explotar();
        }
    }

    public void Explotar()
    {
        // Obtener todas las colisiones en el radio de explosi�n
        Collider[] colliders = Physics.OverlapSphere(transform.position, radioExplosion);

        // Aplicar da�o o efectos a los objetos afectados
        foreach (Collider collider in colliders)
        {
            if (collider.CompareTag("Player"))
            {
                Player_Movement jugador = collider.GetComponent<Player_Movement>();
                if (jugador.barreraActivada == true)
                {
                    jugador.recibirDa�o(0);
                }
                else
                {
                    jugador.recibirDa�o(50);
                }
            }
            else if (collider.CompareTag("Enemy") || collider.CompareTag("EnemyGrande"))
            {
                // Obtener el script del enemigo y destruirlo
                Enemy_Movement enemigo = collider.GetComponent<Enemy_Movement>();
                enemigo.recibirDa�o(100);
            }
            else
            {
                Rigidbody rb = collider.GetComponent<Rigidbody>();
                if (rb != null)
                {
                    // Aplicar fuerza explosiva a los objetos con Rigidbody
                    rb.AddExplosionForce(fuerzaExplosion, transform.position, radioExplosion);
                }
            }
        }

        Instantiate(explosionPrefab, transform.position, Quaternion.identity);
        // Destruir la bomba
        Destroy(gameObject);
    }

}
