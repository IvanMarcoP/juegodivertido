using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Enemy_Movement : MonoBehaviour
{
    public float hp;
    public int velocidad;
    public int puntosGanados;
    private GameObject jugador;
    private GameManagerWave gmw;
    private bool destruido = false;

    // Start is called before the first frame update
    void Start()
    {
        jugador = GameObject.Find("Player");
        gmw = GameObject.Find("GameManagerWave").GetComponent<GameManagerWave>();
    }

    // Update is called once per frame
    void Update()
    {
        transform.LookAt(jugador.transform);
        transform.Translate(velocidad * Vector3.forward * Time.deltaTime);
    }

    public void recibirDa�o(float da�o)
    {
        if (destruido) return;

        hp -= da�o;

        if (hp <= 0)
        {
            destruido = true;
            this.desaparecer();
        }
    }

    private void desaparecer()
    {
        Vector3 posicionEnemigo = transform.position;

        Destroy(gameObject);

        if(gmw.puntos + puntosGanados >= gmw.limitePuntos)
        {
            gmw.puntos = gmw.limitePuntos;
        }
        else
        {
            gmw.agregarPuntos(puntosGanados);
        }
        gmw.instanciarBotiquines(posicionEnemigo);
        gmw.instanciarEscudos();
    }

    //private void OnTriggerEnter(Collider other)
    //{
    //    if (other.CompareTag("Bala"))
    //    {
    //        recibirDa�o(10);
    //    }
    //}
}
