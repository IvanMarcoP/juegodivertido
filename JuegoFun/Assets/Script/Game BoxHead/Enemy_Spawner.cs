using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_Spawner : MonoBehaviour
{
    // referencia al codigo que controla el ciclo de juego
    public GameManagerWave ControlJuego;


    // Listas de enemigos para diferentes rondas
    public List<EnemyInfo> lstEnemySpawnWave1;
    public List<EnemyInfo> lstEnemySpawnWave2;
    public List<EnemyInfo> lstEnemySpawnWave3;
    public List<EnemyInfo> lstEnemySpawnWave4;
    public List<EnemyInfo> lstEnemySpawnWave5;
    public List<EnemyInfo> lstEnemySpawnWave6;

    // Prefab de enemigo tipo bomba
    public GameObject bombaEnemy;

    // Posiciones donde aparecer�n los enemigos
    public Transform[] spawnPosiciones;

   
    // Prefab del jefe final
    public GameObject jefePrefab;

    // Posici�n donde aparecer� el jefe
    [SerializeField] Transform posicionJefe;

    // Referencia al gestor de la tienda
    [SerializeField] Manager_Tienda Tienda;

    // Intervalo de aparici�n de enemigos tipo bomba
    public float intervaloEnemy = 3.5f;

    // Lista de enemigos generados actualmente
    private List<GameObject> spawnedEnemies = new List<GameObject>();

    // Lista temporal de enemigos para la ronda actual
    List<EnemyInfo> lstEnemigosRonda = new List<EnemyInfo>();

   

    void Start()
    {
        // Iniciar las corrutinas para generar enemigos, bombas y decrementar el contador de tiempo
        StartCoroutine(spawnEnemigos());
        StartCoroutine(spawnBomba());
    
    }

    void Update()
    {
        /* Si el contador llega a 0 
       // if (contador <= 0 && Tienda.tiendaActivada)
        {
            contador = 0;          
        }

        // Si el contador es mayor que 0, desactivar el bot�n de mejoras
        if (contador > 0)
        {
            mt.botonMejoras.interactable = false;
        }*/
    }

    public IEnumerator spawnEnemigos()
    {
        // Cambiar la lista de enemigos seg�n la ronda actual
        switch (ControlJuego.rondasPasadas+1)
        {
            case 1:
                lstEnemigosRonda = lstEnemySpawnWave1;
                break;
            case 2:
                lstEnemigosRonda = lstEnemySpawnWave2;
                break;
            case 3:
                lstEnemigosRonda = lstEnemySpawnWave3;
                break;
            case 4:
                lstEnemigosRonda = lstEnemySpawnWave4;
                break;
            case 5:
                lstEnemigosRonda = lstEnemySpawnWave5;
                break;        
        }

        // Generar enemigos mientras el contador sea mayor a 0
        while (ControlJuego.contador > 0)
        {
            foreach (EnemyInfo enemyInfo in lstEnemigosRonda)
            {
                // Obtener el prefab del enemigo y su intervalo de tiempo de aparici�n
                GameObject enemyPrefab = enemyInfo.enemigoPrefab;
                float spawnIntervalo = enemyInfo.intervaloTiempo;

                // Generar enemigos en cada posici�n de spawn
                for (int i = 0; i < spawnPosiciones.Length; i++)
                {
                    if (ControlJuego.contador <= 0)
                    {
                        // Si el contador llega a 0, limpiar enemigos restantes y pasar a la siguiente ronda
                        LimpiarEnemigosRestantes();                        
                        yield break;

                    }

                    // Generar un enemigo en la posici�n actual
                    SpawnEnemy(enemyPrefab, spawnPosiciones[i].position);
                    yield return new WaitForSeconds(spawnIntervalo); // Esperar el intervalo especificado
                }
            }
        }
    }

    // Funci�n para instanciar enemigos y agregarlos a la lista
    void SpawnEnemy(GameObject enemyPrefab, Vector3 spawnPosition)
    {
        GameObject enemy = Instantiate(enemyPrefab, spawnPosition, Quaternion.identity);
        spawnedEnemies.Add(enemy);
    }

    // Funci�n para destruir todos los enemigos restantes en la escena
   public void LimpiarEnemigosRestantes()
    {
        foreach (GameObject enemy in spawnedEnemies)
        {
            Destroy(enemy); // Destruir cada enemigo
        }
        spawnedEnemies.Clear(); // Vaciar la lista de enemigos generados
    }

    public IEnumerator spawnBomba()
    {
        // Generar enemigos tipo bomba mientras el contador sea mayor a 0
        while (ControlJuego.contador > 0)
        {
            foreach (Transform spawnPosicion in spawnPosiciones)
            {
                Instantiate(bombaEnemy, spawnPosicion.position, Quaternion.identity); // Generar la bomba
                yield return new WaitForSeconds(intervaloEnemy); // Esperar el intervalo especificado

                if (ControlJuego.contador <= 0)
                {
                    yield break; // Detener la corrutina si el contador llega a 0
                }
            }
        }
    }

    
}

    [System.Serializable]
    public class EnemyInfo
{
    // Informaci�n de los enemigos: prefab y tiempo de aparici�n
    public GameObject enemigoPrefab;
    public float intervaloTiempo;
}
