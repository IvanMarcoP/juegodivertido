using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using TMPro;
using System.Security.Claims;

public class ControlDialogos : MonoBehaviour
{
    // REFERENCIA AL CICLO DE JUEGO
    public GameManagerWave ControladorJuego;
    public Manager_Tienda managerTienda;


    // REFERENCIA AL JUGADOR y PERSONAJES
    public bool Dialogando;
    public int Opciones;
    public bool Felicitar;
    public bool PresentarSigEnemigo;

    public int Instruccion = 1;

    public int EstadoAnimo;

    // INTERFAZ DE DIALOGOS
    public List<Button> ListaDeBotones; // para las caritas y dar siguiente
    public List<Sprite> ListaDeImagenes; // Listado de imagenes
    [TextArea]
    public List <String>  ListaDeTextos; // Lista de textos

    public GameObject PanelActual;
    public TMP_Text TextoActual;
    
    void Start()
    {
       EstadoAnimo = 0;
       Opciones = 1;
       Dialogando = true;
       IniciarDialogo();
    }

   
    void Update()
    {


        if (Dialogando)
        {
            IniciarDialogo();
        }

    }


    private void IniciarDialogo()
    {
        // pausar el juego 
        ControladorJuego.juegoPausado = true;

        switch (Opciones)
        {
            case 1: 

                //cargamos la imagen correspondiente
                PanelActual.GetComponent<Image>().sprite = ListaDeImagenes[0];

                // cargamos el texto correspondiente
                TextoActual.text = ListaDeTextos[0];

                // mostramos todo
                MostrarDialogos();

                Opciones = 0;
            
                break; // bienvenida al jugador presentacion

            case 2:
                

                break; // pregunta como se siente el jugador y activa opciones

            case 3:


                SeleccionarRespuesta1();


                break; // el jugador se siente feliz

            case 4:

                SeleccionarRespuesta2();

                break; // el jugador se siente triste

            case 5:

                SeleccionarRespuesta3();

                break;  // el jugador se siente enojado

            case 6: 

                SeleccionarRespuesta4();

            break; // el jugador se siente serio

            default:
             


                break;
        }

        if (Felicitar)
        {
            //cargamos la imagen correspondiente
            PanelActual.GetComponent<Image>().sprite = ListaDeImagenes[0];

            // cargamos el texto de felicitaciones y anunciamos proxima ronda
            TextoActual.text = ListaDeTextos[7];

            // mostramos todo
            MostrarDialogos();

            Felicitar = false;
        }    


    }

    public void BotonContinuar()
    {
        if (ControladorJuego.rondasPasadas == 0)
        {
            switch (EstadoAnimo)
            {

                case 0:

                    //cargamos la imagen correspondiente
                    PanelActual.GetComponent<Image>().sprite = ListaDeImagenes[0];

                    // cargamos el texto correspondiente
                    TextoActual.text = ListaDeTextos[1];

                    // mostramos todo
                    //MostrarDialogos();
                    MostrarBotonesCaritas();
                    ListaDeBotones[0].gameObject.SetActive(false);

                    break;   // preguntamos como se siente el jugador mostramos caritas

                case 1:

                    // presentacion enemigo 1 texto e imagen
                    TextoActual.text = ListaDeTextos[6];
                    PanelActual.GetComponent<Image>().sprite = ListaDeImagenes[1];

                    // valor para finalizar el dialogo la proxima vez que el jugador presione el boton continuar
                    EstadoAnimo = 100;


                    break;

                case 2:

                    // presentacion enemigo 1 texto e imagen
                    TextoActual.text = ListaDeTextos[6];
                    PanelActual.GetComponent<Image>().sprite = ListaDeImagenes[1];

                    // valor para finalizar el dialogo la proxima vez que el jugador presione el boton continuar
                    EstadoAnimo = 100;


                    break; // pregunta como se siente el jugador y activa opciones

                case 3:

                    // presentacion enemigo 1 texto e imagen
                    TextoActual.text = ListaDeTextos[6];
                    PanelActual.GetComponent<Image>().sprite = ListaDeImagenes[1];

                    // valor para finalizar el dialogo la proxima vez que el jugador presione el boton continuar
                    EstadoAnimo = 100;


                    break; // pregunta como se siente el jugador y activa opciones

                case 4:

                    // presentacion enemigo 1 texto e imagen
                    TextoActual.text = ListaDeTextos[6];
                    PanelActual.GetComponent<Image>().sprite = ListaDeImagenes[1];

                    // valor para finalizar el dialogo la proxima vez que el jugador presione el boton continuar
                    EstadoAnimo = 100;

                    break; // pregunta como se siente el jugador y activa opciones

                case 5:


                    break;

                case 6:


                    break;

                default:



                case 100:

                    FinalizarDialogo();
                    Opciones = 0;
                    EstadoAnimo = 0;

                    break; // pregunta como se siente el jugador y activa opciones


            }
        }
       
        if (PresentarSigEnemigo)
        {
         
         switch (ControladorJuego.rondasPasadas)
         
         {

                case 0:


                    break;

                case 1:
                    // Primera presentaci�n de enemigo con un flujo de instrucciones dividido en pasos
                    if (Instruccion == 1)
                    {
                        // Instrucci�n 1: Presentaci�n del enemigo con texto e imagen
                        TextoActual.text = ListaDeTextos[8]; // Asignamos el texto correspondiente
                        PanelActual.GetComponent<Image>().sprite = ListaDeImagenes[2]; // Asignamos la imagen correspondiente
                        MostrarDialogos(); // Mostramos los elementos visuales del di�logo

                        Instruccion = 2; // Cambiamos el estado a la siguiente instrucci�n
                    }
                    else if (Instruccion == 2)
                    {
                        // Instrucci�n 2: Finalizaci�n del di�logo
                        Dialogando = false; // Marcamos que ya no estamos en di�logo
                        FinalizarDialogo(); // Llamamos a la funci�n para finalizar el di�logo
                        PresentarSigEnemigo = false; // Marcamos que no hay otro enemigo que presentar
                        Instruccion = 0; // Reiniciamos el estado de las instrucciones

                        // abrir la tienda
                        managerTienda.abrirTienda();
                    }
                    break;

                case 2:

                    // Primera presentaci�n de enemigo con un flujo de instrucciones dividido en pasos
                    if (Instruccion == 1)
                    {
                        // Instrucci�n 1: Presentaci�n del enemigo con texto e imagen
                        TextoActual.text = ListaDeTextos[9]; // Asignamos el texto correspondiente
                        PanelActual.GetComponent<Image>().sprite = ListaDeImagenes[3]; // Asignamos la imagen correspondiente
                        MostrarDialogos(); // Mostramos los elementos visuales del di�logo

                        Instruccion = 2; // Cambiamos el estado a la siguiente instrucci�n
                    }
                    else if (Instruccion == 2)
                    {
                        // Instrucci�n 2: Finalizaci�n del di�logo
                        Dialogando = false; // Marcamos que ya no estamos en di�logo
                        FinalizarDialogo(); // Llamamos a la funci�n para finalizar el di�logo
                        PresentarSigEnemigo = false; // Marcamos que no hay otro enemigo que presentar
                        Instruccion = 0; // Reiniciamos el estado de las instrucciones

                        // abrir la tienda
                        managerTienda.abrirTienda();
                    }


                    break; // pregunta como se siente el jugador y activa opciones

                case 3:

                    // Primera presentaci�n de enemigo con un flujo de instrucciones dividido en pasos
                    if (Instruccion == 1)
                    {
                        // Instrucci�n 1: Presentaci�n del enemigo con texto e imagen
                        TextoActual.text = ListaDeTextos[10]; // Asignamos el texto correspondiente
                        PanelActual.GetComponent<Image>().sprite = ListaDeImagenes[5]; // Asignamos la imagen correspondiente
                        MostrarDialogos(); // Mostramos los elementos visuales del di�logo

                        Instruccion = 2; // Cambiamos el estado a la siguiente instrucci�n
                    }
                    else if (Instruccion == 2)
                    {
                        // Instrucci�n 2: Finalizaci�n del di�logo
                        Dialogando = false; // Marcamos que ya no estamos en di�logo
                        FinalizarDialogo(); // Llamamos a la funci�n para finalizar el di�logo
                        PresentarSigEnemigo = false; // Marcamos que no hay otro enemigo que presentar
                        Instruccion = 0; // Reiniciamos el estado de las instrucciones

                        // abrir la tienda
                        managerTienda.abrirTienda();
                    }




                    break; // pregunta como se siente el jugador y activa opciones



                case 4:

                    // Primera presentaci�n de enemigo con un flujo de instrucciones dividido en pasos
                    if (Instruccion == 1)
                    {
                        // Instrucci�n 1: Presentaci�n del enemigo con texto e imagen
                        TextoActual.text = ListaDeTextos[11]; // Asignamos el texto correspondiente
                        PanelActual.GetComponent<Image>().sprite = ListaDeImagenes[6]; // Asignamos la imagen correspondiente
                        MostrarDialogos(); // Mostramos los elementos visuales del di�logo

                        Instruccion = 2; // Cambiamos el estado a la siguiente instrucci�n
                    }
                    else if (Instruccion == 2)
                    {
                        // Instrucci�n 2: Finalizaci�n del di�logo
                        Dialogando = false; // Marcamos que ya no estamos en di�logo
                        FinalizarDialogo(); // Llamamos a la funci�n para finalizar el di�logo
                        PresentarSigEnemigo = false; // Marcamos que no hay otro enemigo que presentar
                        Instruccion = 0; // Reiniciamos el estado de las instrucciones

                        // abrir la tienda
                        managerTienda.abrirTienda();
                    }




                    break; // pregunta como se siente el jugador y activa opciones


                default:



                case 100:

                    FinalizarDialogo();
                    Opciones = 0;
                    EstadoAnimo = 0;

                    break; // pregunta como se siente el jugador y activa opciones


            }

        }

      

    }

    private void FinalizarDialogo()
    {
        // limpiar textos y desactivar la interfaz

        Opciones = 0;
        TextoActual.text = "";
        PanelActual.SetActive(false);       
        TextoActual.gameObject.SetActive(false);
        ListaDeBotones[0].gameObject.SetActive(false);     
        ControladorJuego.juegoPausado = false; // Restablece el tiempo quitar pausa
        Dialogando = false;
    }

    private void MostrarDialogos()
    {

     // activar panel y texto para que sean visibles
     PanelActual.SetActive(true); // Panel de imagen   
     TextoActual.gameObject.SetActive(true); // texto actual
     ListaDeBotones[0].gameObject.SetActive(true); // boton "continuar"
    
    }

    private void OcultarBotonesCaritas()
    {
        Opciones = 0;

        // desactivamos los objetos para que no sean visibles hasta tanto no se los necesite
        ListaDeBotones[0].gameObject.SetActive(false); // Panel dialogo npc
        ListaDeBotones[1].gameObject.SetActive(false); // Panel dialogo npc
        ListaDeBotones[2].gameObject.SetActive(false); // Panel dialogo npc
        ListaDeBotones[3].gameObject.SetActive(false); // Panel dialogo npc
        ListaDeBotones[4].gameObject.SetActive(false); // Panel dialogo npc

        ListaDeBotones[0].gameObject.SetActive(true);


    }

    private void MostrarBotonesCaritas()
    {
        // desactivar boton de continuar
        ListaDeBotones[0].gameObject.SetActive(false);

        // desactivamos los objetos para que no sean visibles hasta tanto no se los necesite
        ListaDeBotones[0].gameObject.SetActive(true); // Panel dialogo npc
        ListaDeBotones[1].gameObject.SetActive(true); // Panel dialogo npc
        ListaDeBotones[2].gameObject.SetActive(true); // Panel dialogo npc
        ListaDeBotones[3].gameObject.SetActive(true); // Panel dialogo npc
        ListaDeBotones[4].gameObject.SetActive(true); // Panel dialogo npc



    }

    public void SeleccionarRespuesta1()
    {
        // desactivamos lo inecesario y le contestamos al jugador
        OcultarBotonesCaritas();
        Opciones = 0;

    
        TextoActual.text = ListaDeTextos[2];
        EstadoAnimo = 1;
        ListaDeBotones[0].gameObject.SetActive (true);

    }

    public void SeleccionarRespuesta2()
    {
        // desactivamos lo inecesario y le contestamos al jugador
        OcultarBotonesCaritas();
        Opciones = 0;


        TextoActual.text = ListaDeTextos[3];
        EstadoAnimo = 2;
        ListaDeBotones[0].gameObject.SetActive(true);
    }

    public void SeleccionarRespuesta3()
    {
        // desactivamos lo inecesario y le contestamos al jugador
        OcultarBotonesCaritas();
        Opciones = 0;


        TextoActual.text = ListaDeTextos[4];
        EstadoAnimo = 3;
        ListaDeBotones[0].gameObject.SetActive(true);
    }

    public void SeleccionarRespuesta4()
    {
        // desactivamos lo inecesario y le contestamos al jugador
        OcultarBotonesCaritas();
        Opciones = 0;


        TextoActual.text = ListaDeTextos[5];
        EstadoAnimo = 4;
        ListaDeBotones[0].gameObject.SetActive(true);
    }
}

// AQUI SE DETALLA LA LISTA DE TEXTOS PARA TENER UNA GUIA DE SU USO
/*
Lista de textos
1
2
3
4
5
6
7
8
9
10
11
12
13
14
15
 



 
 
 
 
 */
