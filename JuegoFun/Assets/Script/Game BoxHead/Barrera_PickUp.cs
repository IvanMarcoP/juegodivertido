using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Barrera_PickUp : MonoBehaviour
{
    public GameObject barreraPrefab;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            GameObject jugador = other.gameObject;

            GameObject barrera = Instantiate(barreraPrefab, jugador.transform.position, Quaternion.identity);
            barrera.transform.parent = jugador.transform;

            Destroy(gameObject);
        }
    }
}
