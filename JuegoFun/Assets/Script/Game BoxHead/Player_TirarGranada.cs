﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_TirarGranada : MonoBehaviour
{
    [Header("Datos Granada")]
    public GameObject granadaPrefab;
    public GameObject explosionPrefab;
    public float fuerzaLanzamiento;
    public float tiempoDetonacion;
    public float radioExplosion = 5f;
    public float fuerzaExplosion = 100f;
    public int cantidadGranadas = 10;
    public Transform posicionLanzamiento;
    public float dpsBomba = 100;

    // Update is called once per frame
    void Update()
    {
        if(cantidadGranadas > 0)
        {
            if (Input.GetKeyDown(KeyCode.E))
            {
                LanzarGranada();
                cantidadGranadas--;
            }
        }
    }

    private void LanzarGranada()
    {
        GameObject granada = Instantiate(granadaPrefab, posicionLanzamiento.position, Quaternion.identity);
        Rigidbody granadaRb = granada.GetComponent<Rigidbody>();

        Vector3 direccionLanzamiento = transform.forward;

        granadaRb.AddForce(direccionLanzamiento * fuerzaLanzamiento, ForceMode.VelocityChange);

        granadaRb.rotation = Quaternion.LookRotation(direccionLanzamiento);
        // Detonar la granada despu�s de cierto tiempo
        StartCoroutine(DetonarGranada(granada, tiempoDetonacion));
    }

    private IEnumerator DetonarGranada(GameObject granada, float tiempo)
    {
        yield return new WaitForSeconds(tiempo);

        // Obtener todas las colisiones en el radio de explosión
        Collider[] colliders = Physics.OverlapSphere(granada.transform.position, radioExplosion);

        // Aplicar daño o efectos a los objetos afectados
        foreach (Collider collider in colliders)
        {
            if (collider.CompareTag("Enemy") || collider.CompareTag("EnemyGrande") || collider.CompareTag("EnemyFast"))
            {
                Enemy_Movement enemigo = collider.GetComponent<Enemy_Movement>();
                if (enemigo != null) enemigo.recibirDaño(dpsBomba);
            }
            else if (collider.CompareTag("EnemyBomba"))
            {
                Bomba_Movement enemy = collider.GetComponent<Bomba_Movement>();
                if (enemy != null) enemy.recibirDaño(dpsBomba);
            }
            else if (collider.CompareTag("ViejoVerde"))
            {
                ViejoVerde enemy = collider.GetComponent<ViejoVerde>();
                if (enemy != null) enemy.recibirDaño(dpsBomba);
            }
            else if (collider.CompareTag("DobleCara"))
            {
                Enemy_Cambiaformas enemy = collider.GetComponent<Enemy_Cambiaformas>();
                if (enemy != null) enemy.recibirDaño(dpsBomba);
            }
            else if (collider.CompareTag("viejaHolograma"))
            {
                ViejaHologramas enemy = collider.GetComponent<ViejaHologramas>();
                if (enemy != null) enemy.recibirDaño(dpsBomba);
            }
            else
            {
                Rigidbody rb = collider.GetComponent<Rigidbody>();
                if (rb != null)
                {
                    // Aplicar fuerza explosiva a los objetos con Rigidbody
                    rb.AddExplosionForce(fuerzaExplosion, granada.transform.position, radioExplosion);
                }
            }
        }

        Instantiate(explosionPrefab, granada.transform.position, Quaternion.identity);
        // Destruir la granada
        Destroy(granada);
    }
}

// || collider.CompareTag("EnemyGrande") || collider.CompareTag("EnemyFast")