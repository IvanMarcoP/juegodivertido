using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_Cambiaformas : MonoBehaviour
{
    public float hp;
    public int velocidad;
    public int puntosGanados;
    private GameObject jugador;
    private GameManagerWave gmw;
    private bool destruido = false;

    public float raycastDistance = 10f; // La distancia del raycast
    public LayerMask raycastLayerMask;  // Las capas que el raycast puede detectar (evita otras capas como el suelo)

   
    public GameObject formaBuena;
    public GameObject formaMala;
    void Start()
    {
        jugador = GameObject.Find("Player");
        gmw = GameObject.Find("GameManagerWave").GetComponent<GameManagerWave>();
    }

    // Update is called once per frame
    void Update()
    {
        transform.LookAt(jugador.transform);
        transform.Translate(velocidad * Vector3.forward * Time.deltaTime);

        RaycastForward();
    }

    public void RaycastForward()
    {
        
        RaycastHit hit;
        if (Physics.Raycast(transform.position, transform.forward, out hit, raycastDistance, raycastLayerMask))
        {
           
            if (hit.collider.CompareTag("JugadorAdelante"))
            {
                Debug.Log("Golpe� al jugador adelante");
                formaBuena.SetActive(true);
                formaMala.SetActive(false);
            }
         
            else if (hit.collider.CompareTag("JugadorAtras"))
            {
                formaBuena.SetActive(false);
                formaMala.SetActive(true);
            }
        }
        else
        {
            Debug.Log("No golpe� a ning�n jugador");
          
        }
    
        Debug.DrawRay(transform.position, transform.forward * raycastDistance, Color.red);
    }

    public void recibirDa�o(float da�o)
    {
        if (destruido) return;

        hp -= da�o;

        if (hp <= 0)
        {
            destruido = true;
            this.desaparecer();
        }
    }

    private void desaparecer()
    {
        Vector3 posicionEnemigo = transform.position;

        Destroy(gameObject);

        if (gmw.puntos + puntosGanados >= gmw.limitePuntos)
        {
            gmw.puntos = gmw.limitePuntos;
        }
        else
        {
            gmw.agregarPuntos(puntosGanados);
        }
        gmw.instanciarBotiquines(posicionEnemigo);
        gmw.instanciarEscudos();
    }

    //private void OnTriggerEnter(Collider other)
    //{
    //    if (other.CompareTag("Bala"))
    //    {
    //        recibirDa�o(10);
    //    }
    //}
}
