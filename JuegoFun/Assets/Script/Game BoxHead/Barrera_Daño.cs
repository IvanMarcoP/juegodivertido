using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Barrera_Daño : MonoBehaviour
{
    public float barreraDaño = 100f;
    private Player_Movement playerMovement;

    void Start()
    {
        playerMovement = FindObjectOfType<Player_Movement>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Enemy") || other.CompareTag("EnemyGrande"))
        {
            Enemy_Movement enemigo = other.GetComponent<Enemy_Movement>();
            if (enemigo != null)
            {
                enemigo.recibirDaño(barreraDaño);
            }

            Destroy(gameObject);
            playerMovement.DesactivarBarrera();
        }
        else if (other.CompareTag("EnemyBomba"))
        {
            Bomba_Movement enemigoB = other.GetComponent<Bomba_Movement>();
            if (enemigoB != null)
            {
                enemigoB.recibirDaño(barreraDaño);
            }

            Destroy(gameObject);
            playerMovement.DesactivarBarrera();
        }
        else if (other.CompareTag("ViejoVerde"))
        {
            Bomba_Movement enemigoB = other.GetComponent<Bomba_Movement>();
            if (enemigoB != null)
            {
                enemigoB.recibirDaño(barreraDaño);
            }

            Destroy(gameObject);
            playerMovement.DesactivarBarrera();
        }
        else if (other.CompareTag("viejaHolograma"))
        {
            ViejaHologramas enemigoB = other.GetComponent<ViejaHologramas>();
            if (enemigoB != null)
            {
                enemigoB.recibirDaño(barreraDaño);
            }

            Destroy(gameObject);
            playerMovement.DesactivarBarrera();
        }
    }
}
