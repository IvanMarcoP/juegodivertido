using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Cinemachine;


public class Player_Movement : MonoBehaviour
{
    [Header("Player Stats")]
    public float velocidad;
    public int vida;
    public int vidaMaxima;
    [SerializeField] private Manager_Tienda managerTienda;
    private Rigidbody rb;
    private Player_TirarGranada pg;
    private Player_Shooting ps;
    private Player_LaserGun playerLaser;
    private Camera mainCamara;
    public bool barreraActivada;

    [Header("Aturdimiento del Viejo Verde")]
    public float TiempoAturdido;
    public bool Aturdido;
    private  float timerAturdido=0f;
    private float timerCdAturdimiento = 0f;

    [Header("Habilidad oso")]
    public GameObject osoPrefab;
    public int regerenracionVida;
    public float duracionInvocacion;
    public float cooldownHabilidad;
    private bool osoActivado = false;
    private bool enCooldown = false;

    private Animator animViejoVerde;

    private Animator animPj;
    public bool isMoviendose;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        pg = GetComponent<Player_TirarGranada>();
        ps = GetComponent<Player_Shooting>();
        playerLaser = GetComponent<Player_LaserGun>();

        osoPrefab.SetActive(false);

        mainCamara = FindObjectOfType<Camera>();

        Aturdido = false;
        animPj = this.gameObject.GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        animPj.SetBool("isMoviendose", isMoviendose);

        if (timerAturdido > 0)
        {
            timerAturdido -= Time.deltaTime;
            Aturdido = true;
            if(animViejoVerde!=null)
                animViejoVerde.SetBool("isAtacando", Aturdido);
        }
        else
        {
            Aturdido = false;
            if (animViejoVerde != null)
                animViejoVerde.SetBool("isAtacando", Aturdido);
        }

       

        if (timerCdAturdimiento > 0)
            timerCdAturdimiento -= Time.deltaTime;

        if (!Aturdido)
        {
            movimientoPJ();
        }

        if (Input.GetKeyDown(KeyCode.Q) && osoActivado == false && enCooldown == false)
        {
            ActivarOso();
        }

        mirarCamara();
    }

    void mirarCamara()
    {
        Ray cameraRay = mainCamara.ScreenPointToRay(Input.mousePosition);

        Plane groundPlane = new Plane(Vector3.up, Vector3.zero);
        float rayLength;

        if (groundPlane.Raycast(cameraRay, out rayLength))
        {
            Vector3 pointToLook = cameraRay.GetPoint(rayLength);

            transform.LookAt(new Vector3(pointToLook.x, transform.position.y, pointToLook.z));
        }
    }

    private void movimientoPJ()
    {
        Vector3 vectorInput = new Vector3(0, 0, 0);

        if (Input.GetKey(KeyCode.W))
        {
            vectorInput.z = +1;
           
        }
        if (Input.GetKey(KeyCode.S))
        {
            vectorInput.z = -1;
           
        }
        if (Input.GetKey(KeyCode.A))
        {
            vectorInput.x = -1;
           
        }
        if (Input.GetKey(KeyCode.D))
        {
            vectorInput.x = +1;
            
        }

        if(Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.S) ||
            Input.GetKey(KeyCode.A)|| Input.GetKey(KeyCode.D))
            isMoviendose = true;
        else
        {
            isMoviendose = false;
        }

        vectorInput = vectorInput.normalized;

        Vector3 moverDireccion = new Vector3(vectorInput.x, 0, vectorInput.z);
        transform.position += moverDireccion * velocidad * Time.deltaTime;


    }

    void ActivarOso()
    {
        osoPrefab.SetActive(true);
        StartCoroutine(HabilidadOso());
    }

    private IEnumerator HabilidadOso()
    {
        osoActivado = true;
        float tiempoRestante = duracionInvocacion;

        while (tiempoRestante > 0)
        {
            tiempoRestante -= 1f;
            aumentarVida(regerenracionVida);
            yield return new WaitForSeconds(1f);
        }

        osoPrefab.SetActive(false);
        osoActivado = false;
        StartCoroutine(InicioCooldown());
    }

    private IEnumerator InicioCooldown()
    {
        enCooldown = true;
        yield return new WaitForSeconds(cooldownHabilidad);
        enCooldown = false;
    }

    public void recibirDa�o(float da�o)
    {
        if (barreraActivada)
        {
            vida = vida - 0;
        }

        vida = vida - Mathf.RoundToInt(da�o);
    }

    private void aumentarVida(int curacion)
    {
        vida = Mathf.Min(vida + curacion, vidaMaxima);
    }


    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("ViejoVerde"))
        {
            IniciarAturdimiento(collision);                 
        }

        if (collision.gameObject.CompareTag("EnemyGrande"))
        { recibirDa�o(20); }

        if (collision.gameObject.CompareTag("Enemy") || collision.gameObject.CompareTag("EnemyFast"))

        {
            recibirDa�o(10);
        }

        if (collision.gameObject.CompareTag("DobleCara"))
        { recibirDa�o(15); }        

        if (collision.gameObject.CompareTag("viejaHolograma"))
        { recibirDa�o(15); }
    }

    public void ActivarBarrera()
    {
        barreraActivada = true;
    }

    public void DesactivarBarrera()
    {
        barreraActivada = false;
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Botiquin"))
        {
            aumentarVida(25);
            pg.cantidadGranadas += 2;
            ps.metralletaBalas += 20;
            ps.escopetaBalas += 8;
            ps.misilesBalas += 3;
            playerLaser.laserEnergia += 20;
            Destroy(other.gameObject);
        }


        if (other.gameObject.CompareTag("Barrera"))
        {
            ActivarBarrera();
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.CompareTag("ViejoVerde"))
        {
            animViejoVerde = collision.gameObject.GetComponent<Animator>();            
            animViejoVerde.SetBool("isAtacando", Aturdido);
        }
    }

    // COMPORTAMIENTOS ESPECIFICOS DEL JUGADOR CONTRA NUEVOS ENEMIGOS

    private void IniciarAturdimiento(Collision collision)
    {      
        if (timerCdAturdimiento <=0)
        {         
            timerAturdido = 3f;
            timerCdAturdimiento = 6f;
        }
        animViejoVerde = collision.gameObject.GetComponent<Animator>();      
    }



}
