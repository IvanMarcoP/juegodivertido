using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ViejaHologramas : MonoBehaviour
{
    public float hp;
    public int velocidad;
    public int puntosGanados;
    private GameObject jugador;
    private GameManagerWave gmw;
    private bool destruido = false;

    public float raycastDistance = 10f; // La distancia del raycast
    public LayerMask raycastLayerMask;  // Las capas que el raycast puede detectar (evita otras capas como el suelo)

    public float radioDeteccion = 5f; // Radio de detecci�n del enemigo
    private bool jugadorEnRango = false; // Indica si el jugador est� en el rango

    private Animator animator;
    private bool isAtacando;
    void Start()
    {
        jugador = GameObject.Find("Player");
        gmw = GameObject.Find("GameManagerWave").GetComponent<GameManagerWave>();
        animator = this.gameObject.GetComponent<Animator>();
    }

   
    void Update()
    {
        transform.LookAt(jugador.transform);

        if (!jugadorEnRango)
        {         
            transform.Translate(velocidad * Vector3.forward * Time.deltaTime);
        }

        // Reseteamos el estado antes de revisar
        jugadorEnRango = false;

        // Detectamos colisiones dentro del radio
        Collider[] colisiones = Physics.OverlapSphere(transform.position, radioDeteccion);
        foreach (var colision in colisiones)
        {
            if (colision.CompareTag("Player"))
            {
                jugadorEnRango = true;
                Debug.Log("Jugador detectado dentro del radio del enemigo.");
                break; // Detenemos el bucle al encontrar al jugador
            }
        }

        // Acciones seg�n el estado de jugadorEnRango
        if (jugadorEnRango)
        {
            isAtacando = true;
            animator.SetBool("isAtacando", isAtacando);
        }
        else
        {
            isAtacando = false;
            animator.SetBool("isAtacando", isAtacando);
        }
    }

    private void OnDrawGizmosSelected()
    {
        // Dibuja el radio de detecci�n en el editor para visualizar el alcance
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, radioDeteccion);
    }

    public void recibirDa�o(float da�o)
    {
        if (destruido) return;

        hp -= da�o;

        if (hp <= 0)
        {
            destruido = true;
            this.desaparecer();
        }
    }

    private void desaparecer()
    {
        Vector3 posicionEnemigo = transform.position;

        Destroy(gameObject);

        if (gmw.puntos + puntosGanados >= gmw.limitePuntos)
        {
            gmw.puntos = gmw.limitePuntos;
        }
        else
        {
            gmw.agregarPuntos(puntosGanados);
        }
        gmw.instanciarBotiquines(posicionEnemigo);
        gmw.instanciarEscudos();
    }

    //private void OnTriggerEnter(Collider other)
    //{
    //    if (other.CompareTag("Bala"))
    //    {
    //        recibirDa�o(10);
    //    }
    //}
}
