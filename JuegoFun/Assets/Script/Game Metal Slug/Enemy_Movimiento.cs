using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_Movimiento : MonoBehaviour
{
    public int hp;
    public int velocidad;
    private GameObject jugador; 

    // Start is called before the first frame update
    void Start()
    {
        jugador = GameObject.Find("Player");
    }

    // Update is called once per frame
    void Update()
    {
        transform.LookAt(jugador.transform); 
        transform.Translate(velocidad * Vector3.forward * Time.deltaTime);
    }

    public void recibirDaņoBala() 
    {
        hp = hp - 25; 
        
        if (hp <= 0) 
        { 
            this.desaparecer(); 
        }
    }

    public void recibirDaņoGranada()
    {
        hp = hp - 50;

        if (hp <= 0)
        {
            this.desaparecer();
        }
    }

    private void desaparecer() 
    {
        Destroy(gameObject);
        Hud_Controller.instance.aņadirPuntos();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Bala"))
        {
            recibirDaņoBala();
        }
        //if (other.CompareTag("Granada"))
        //{
        //    recibirDaņoGranada();
        //}
    }
}
