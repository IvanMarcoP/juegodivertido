using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bomba_Movement : MonoBehaviour
{
    public float hp = 50f;
    public int velocidad = 5;
    public int puntosGanados = 1000;
    private GameObject jugador;
    private GameManagerWave gmw;
    private bool destruido = false;
    [SerializeField] Enemy_Bomba enemy_Bomba;

    // Start is called before the first frame update
    void Start()
    {
        jugador = GameObject.Find("Player");
        gmw = GameObject.Find("GameManagerWave").GetComponent<GameManagerWave>();
    }

    // Update is called once per frame
    void Update()
    {
        transform.LookAt(jugador.transform);
        transform.Translate(velocidad * Vector3.forward * Time.deltaTime);
    }

    public void recibirDa�o(float da�o)
    {
        if (destruido) return;

        hp -= da�o;

        if (hp <= 0)
        {
            destruido = true;
            this.desaparecer();
        }
    }

    private void desaparecer()
    {
        Vector3 posicionEnemigo = transform.position;

        enemy_Bomba.Explotar();

        if (gmw.puntos + puntosGanados >= gmw.limitePuntos)
        {
            gmw.puntos = gmw.limitePuntos;
        }
        else
        {
            gmw.agregarPuntos(puntosGanados);
        }

        gmw.instanciarBotiquines(posicionEnemigo);
        gmw.instanciarEscudos();
    }

   

}
