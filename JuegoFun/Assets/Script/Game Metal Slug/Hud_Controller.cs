using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Hud_Controller : MonoBehaviour
{
    public static Hud_Controller instance;

    public TMPro.TMP_Text textoPuntos;
    public TMPro.TMP_Text textoVida;
    public TMPro.TMP_Text textoGameOver;
    [SerializeField] Player_Movimiento pmi;

    int puntos = 0;

    private GameManager gm;

    void Awake()
    {
        instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        textoGameOver.enabled = false;
        gm = GameObject.Find("GameManager").GetComponent<GameManager>();
    }

    // Update is called once per frame
    void Update()
    {
        setearTextos();
        perderJuego();
        
    }

    private void setearTextos()
    {
        textoPuntos.text = puntos.ToString();
        textoVida.text = "1UP = " + pmi.vidas.ToString();
    }

    public void aņadirPuntos()
    {
        puntos = puntos + 100;
    }

    public void perderJuego()
    {
        if (pmi.vidas <= 0)
        {
            GameManager.gameOver = true;
        }

        if (GameManager.gameOver)
        {
            textoGameOver.enabled = true;
            Time.timeScale = 0;
        }
    }
}
